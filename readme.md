# Backend Configurations Setup

### Add typescript

- configuration file => `tsconfig.json`
- have a good base configuration => https://github.com/tsconfig/bases

### Add SWC (Speedy Web Compiler)

- configuration file => `.swcrc`

### Add eslint with typescript

- configuration file => `.eslintrc`
- configuration file for prettier => `.prettierrc`

### Add express

### Add morgan - logging library for express

---

### Dockerise the application

```
- NOTE: we try keeping Docker file as production ready as possible
- NOTE: development commands are all written in docker compose
```

- add `.dockerignore` file
- write a `Dockerfile` to create image for running your node application
- how to run images => docker-compose.yml
- `npm run docker:start` => a script to compile js from ts then start the server
- automate script using `triggertaskonsave` extension
- enable debugging in docker
- add postgres in docker => and check if its working `npm run db:console`

---

### Add knex with pg (postgres)

- configuration file => `knexfile.ts`

#### Migrate

- create a new migration file => `npx knex migrate:make init_db`
- rebuild the backend container => `yarn rebuild:backend`
- start migration in docker container => `yarn docker:db:migrate`

#### Seeder

- create a new seed file => `npx knex seed:make init_data`
- rebuild the backend container => `yarn rebuild:backend`
- start seed in docker container => `yarn docker:db:seed`

```
NOTE: yarn is better since its faster and is easier to debug
```

---

MAIN COMMAND TO EXECUTE => `docker compose up`

Blogpost to follow during the setup => https://kevinwwwade.medium.com/the-ultimate-node-docker-setup-step-by-step-2022-update-2927b8ca547c

---

## Scripts

- `build` => to compile js from ts
- `start` => run the compiled js node server in dist folder
- `docker:start` => build + start the server

- `db:console` => open the postgres psql terminal where we can execute our sql commands
- `rebuild:backend` => any changes in the configuration of the node js server (i.e. package.json file => execute this command so that it's reflected in our docker container as well)

- `db:migrate` => to migrate in current local directory
- `docker:db:migrate` => to migrate in docker container directory
- `db:seed` => to seed in current local directory
- `docker:db:seed` => to seed in docker container directory

---

## Commands to execute on Cloning

- `yarn install`
- `docker compose up`
- `yarn docker:db:migrate`
- `yarn docker:db:seed`
- Execute GET request on: http://localhost:8000/
- check the database content and tables via sql => `yarn db:console`
- `docker compose down`

---

## Authentication Cycle

1. Create a user with username and password
   - password must be hashed using bcrypt => `bcrypt.hash(password, saltNo)`
2. Login user with username and password => this method is called authenticating the user
   - password must be compared to the stored hashed password => `bcrypt.compare(password, storedPassword)`
