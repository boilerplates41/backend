import models from "../models";
import { UserInterface } from "../interfaces";
import { userService } from "../services";
import exp from "express";

const UserModel = models.User;

const createUser = async (req: exp.Request, res: exp.Response) => {
  const { username, password }: UserInterface = req.body;
  try {
    const newUser = await userService.createUser(username, password);
    res.status(201).json({ success: true, newUser });
  } catch (err) {
    res.status(401).json({ success: false, err });
  }
};

const getAllUsers = async (req: exp.Request, res: exp.Response) => {
  try {
    const allUsers = await UserModel.findAll();
    res.status(201).json({ success: true, allUsers });
  } catch (err) {
    res.status(401).json({ success: false, err });
  }
};

const authenticateUser = async (req: exp.Request, res: exp.Response) => {
  const { username, password }: UserInterface = req.body;
  try {
    const accessToken = await userService.loginUser(username, password);
    res.status(201).json({ success: true, accessToken });
  } catch (err) {
    res.status(401).json({ success: false, err });
  }
};

export default {
  createUser,
  getAllUsers,
  authenticateUser,
};
