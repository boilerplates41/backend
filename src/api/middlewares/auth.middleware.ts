import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";

const jwtSecretKey = process.env.JWT_SECRET || "somecomplexstringdata";

const validateTokenMiddleware = (
  req: Request | any,
  res: Response,
  next: NextFunction
) => {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];

  if (token) {
    jwt.verify(token, jwtSecretKey, (err: any, user: any) => {
      if (err) {
        return res.status(403).send({ msg: "Invalid Access Token!" });
      } else {
        console.log(user);
        req.user = user;
        next();
      }
    });
  } else {
    return res.status(401).send({ msg: "This action is forbidden for you!" });
  }
};

export default validateTokenMiddleware;
