import models from "../models";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";

import dotenv from "dotenv";
dotenv.config();

const saltRounds = Number(process.env.JWT_SALT || 10);
const jwtSecretKey = process.env.JWT_SECRET || "somecomplexstringdata";

const UserModel = models.User;

const createUser = async (username: string, password: string) => {
  try {
    const isUserExists = await UserModel.findOne({ where: { username } });
    if (isUserExists) throw Error("User Already Exists!");
    const hashedPassword = await bcrypt.hash(password, saltRounds);
    const newUser = await UserModel.create({
      username,
      password: hashedPassword,
    });
    return newUser;
  } catch (err: any) {
    throw Error(err);
  }
};

const loginUser = async (username: string, password: string) => {
  const isUserExists = await UserModel.findOne({ where: { username } });
  if (!isUserExists) throw Error("User does not exist! Please signup.");
  const isMatch = bcrypt.compareSync(password, isUserExists.password);
  if (!isMatch) throw Error("Incorrect Password!");

  const payload = {
    username: isUserExists.username,
    id: isUserExists.id,
  };

  const accessToken = jwt.sign(payload, jwtSecretKey);
  return accessToken;
};

export default {
  createUser,
  loginUser,
};
