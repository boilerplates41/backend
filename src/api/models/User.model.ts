/* eslint-disable @typescript-eslint/no-explicit-any */
"use strict";

const userModel = (sequelize: any, DataTypes: any) => {
  const User = sequelize.define(
    "user",
    {
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    { timestamp: false }
  );

  // User.associate = (models: any) => {};

  return User;
};

export default userModel;
