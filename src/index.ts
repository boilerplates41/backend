import express from "express";
import morgan from "morgan";
import db from "./api/models";
import { UserRoute } from "./api/routes";

const app = express();

app.use(morgan("dev"));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/api/vi/user", UserRoute);

const port = Number(process.env.PORT || 8080);
db.sequelize.sync().then(() => {
  app.listen(port, "0.0.0.0", () => {
    console.log(`listening at: http://localhost:${port}`);
  });
});
