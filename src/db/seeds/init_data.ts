import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("users").del();

  // Inserts seed entries
  await knex("users").insert([
    {
      id: 1,
      phonenumber: "9987865432",
      email: "tani@gmail.com",
      password: "tani123",
    },
    {
      id: 2,
      phonenumber: "4534567897",
      email: "yuvi@gmail.com",
      password: "yuvi123",
    },
    {
      id: 3,
      phonenumber: "4534908789",
      email: "mom@gmail.com",
      password: "mom123",
    },
    {
      id: 4,
      phonenumber: "3423567890",
      email: "dad@gmail.com",
      password: "dad123",
    },
  ]);
}
